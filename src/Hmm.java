import static org.junit.Assert.*;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.junit.Test;

class UnitConfig
{
	public int weapon;
	public int armor;
	public int hp;
	public int initiative;
	public int speed;
	public int range;
	
	UnitConfig(int weapon, int armor, int hp, int speed, int initiative, int range)
	{
		this.weapon = weapon;
		this.armor = armor;
		this.hp = hp;
		this.initiative = initiative;
		this.speed = speed;
		this.range = range;
	}
}

class ConfigStorage
{
	public static HashMap<String, UnitConfig> units = initUnitsConfig();
	public static final int NOT_DEFINED = -1;
	
	private static HashMap<String, UnitConfig> initUnitsConfig()
	{
		HashMap<String, UnitConfig> config = new HashMap<String, UnitConfig>();
		
		config.put("Griffin",     new UnitConfig(10,  8,  25,  6,  5, NOT_DEFINED));
		config.put("Cavalier",    new UnitConfig(20, 15, 100,  7,  7, NOT_DEFINED));
		config.put("Stone Golem", new UnitConfig(10,  7,  30,  3,  3, NOT_DEFINED));
		config.put("Giant",       new UnitConfig(40, 20,  50, 11, 10, NOT_DEFINED));
		
		config.put("Archer",         new UnitConfig(3,  3, 25, 3, 5,  5));
		config.put("Arch Mage",      new UnitConfig(15, 2, 20, 2, 10, 10));
		config.put("Monk",           new UnitConfig(17, 1, 20, 2, 11, 12));
		config.put("Master Gremlin", new UnitConfig(1,  3, 30, 5, 4,  3));
		return config;
	}
}

class Logger
{
	
	private static PrintStream out;
	
	public static void setStream(PrintStream out)
	{
		Logger.out = out;
	}
	
	public static void logMove(Unit unit, int new_x, int new_y)
	{
		out.println("log: " + unit + " moved to (" + new_x + ", " + new_y + ")");
	}
	
	public static void logAttack(Unit attacker, Unit attacked, int damage)
	{
		out.println("log: " + attacker + " attacked " + attacked + " (" + damage + " dmg)");
	}
	
	public static void logDie(Unit unit)
	{
		out.println("log: " + unit + " died");
	}
	
	public static void logEndOfBattle()
	{
		out.println("log: end of battle");
	}
}

class GameInterface
{
	private static PrintStream out;
	
	public static void setStream(PrintStream out)
	{
		GameInterface.out = out;
	}
	
	private static void printMoveArray(ArrayList<MapObject> arr) {
		if (arr.size() == 0) {
			out.println("null");
		} else {
			for (int i = 0; i < arr.size() - 1; i++) {
				out.print(arr.get(i) + ", ");
			}
			out.println(arr.get(arr.size() - 1));
		}
	}
	
	public static void printMove(Move move)
	{
		
		out.println(move.unit + ":");
		out.print("  move: ");
		printMoveArray(move.move);
		out.print("  attack: ");
		printMoveArray(move.attack);
	}
}

class MapObject
{
	int x;
	int y;
	
	MapObject(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	@Override 
	public boolean equals(Object other)
	{
		if (other instanceof MapObject) {
			MapObject other_map_obj = (MapObject) other;
			
			return this.x == other_map_obj.x && this.y == other_map_obj.y;
		} 
		return false;
	}
	
	@Override
	public String toString()
	{
		return " (" + x + ", " + y + ")";
	}
}

class Barrier extends MapObject
{

	Barrier(int x, int y) {
		super(x, y);
	}
	
}

abstract class Unit extends MapObject
{
	public int initiative;
	public int id;
	public int speed;
	public int army_id;
	public String name;
	public int hp;
	public boolean is_alive = true;
	
	protected int weapon;
	protected int armor;
	
	Unit(int army_id, int x, int y, String name, int id, int weapon, int armor, int hp, int speed, int initiative)
	{
		super(x, y);
		this.name = name;
		this.id = id;
		this.weapon = weapon;
		this.armor = armor;
		this.hp = hp;
		this.speed = speed;
		this.initiative = initiative;
		this.army_id = army_id;
	}
	
	@Override
	public String toString()
	{
		return name + " (" + x + ", " + y + ")";
	}
	
	protected int damage(Unit other)
	{
		return Math.max(1, this.weapon - other.armor);
	}
	
	public void attack(Unit other)
	{
		if (other.is_alive) {
			int damage = damage(other);
			Logger.logAttack(this, other, damage);
			other.hp -= damage;
			if (other.hp <= 0) {
				other.is_alive = false;
				Logger.logDie(other);
			}
		}
	}
	
	abstract public int getAtackRange();
}

class Melee extends Unit
{

	Melee(int army_id, int x, int y, String name, int id, int weapon, int armor, int hp, int speed, int initiative) {
		super(army_id, x, y, name, id, weapon, armor, hp, speed, initiative);
	}

	@Override
	public int getAtackRange() {
		return 1;
	}
	
}

class StoneGolem extends Melee
{

	StoneGolem(int army_id, int x, int y, String name, int id, int weapon,
			int armor, int hp, int speed, int initiative) {
		super(army_id, x, y, name, id, weapon, armor, hp, speed, initiative);
	}
	
	@Override
	public void attack(Unit other)
	{
		super.attack(other);
		if (other.x  % 2 == other.y % 2) {
			super.attack(other);
		}
	}
}

class Range extends Unit
{
	private final int INC_OF_ARMOR_FOR_MELEE = 2;
	
	private int range;
	
	Range(int army_id, int x, int y, String name, int id, int weapon, int armor, int hp, int speed, int initiative, int range) {
		super(army_id, x, y, name, id, weapon, armor, hp, speed, initiative);
		this.range = range;
	}

	@Override
	public int getAtackRange() {
		return range;
	}
	
	@Override
	protected int damage(Unit other)
	{
		if (other instanceof Melee) {
			return Math.max(1, this.weapon - other.armor - INC_OF_ARMOR_FOR_MELEE);
		}
		return super.damage(other);
	}
}

class UnitFactory
{
	public static Unit genUnit(String name, int army_id, int x, int y, int id)
	{
		UnitConfig config = ConfigStorage.units.get(name);
		if (name == "Stone Golem") return new StoneGolem(army_id, x, y, name, id, config.weapon, config.armor, config.hp, config.speed, config.initiative);
		if (config.range == ConfigStorage.NOT_DEFINED) {
			return new Melee(army_id, x, y, name, id, config.weapon, config.armor, config.hp, config.speed, config.initiative);
		} else {
			
			return new Range(army_id, x, y, name, id, config.weapon, config.armor, config.hp, config.speed, config.initiative,  config.range);
		}
	}
}

class Move
{
	Unit unit;
	ArrayList<MapObject> move;
	ArrayList<MapObject> attack;
	
	Move(Unit unit, ArrayList<MapObject> move, ArrayList<MapObject> attack){
		this.unit = unit;
		this.move = move;
		this.attack = attack;
	}
}

class Game
{
	private final int ARMIES_NUMB = 2;
	
	public boolean is_finished = false;
	private ArrayList< ArrayList<Unit> > armies = new ArrayList< ArrayList<Unit> >();
	private MapObject map[][];
	private int max_x;
	private int max_y;
	private ArrayList<Integer> cur_unit = new ArrayList<Integer>();
	private int cur_army;
	
	Game(int max_x, int max_y, ArrayList<Barrier> barriers, ArrayList<Unit> army_1, ArrayList<Unit> army_2)
	{
		initMap(max_x, max_y, barriers);
		initArmies(army_1, army_2);
	}

	private void initMap(int max_x, int max_y, ArrayList<Barrier> barriers) {
		map = new MapObject[max_x][max_y];
		this.max_x = max_x;
		this.max_y = max_y;
		for (Barrier barrier : barriers) {
			map[barrier.x][barrier.y] = barrier;
		}
	}

	private void initArmies(ArrayList<Unit> army_1, ArrayList<Unit> army_2) {
		armies.add(army_1);
		armies.add(army_2);
		
		for (ArrayList<Unit> army : armies) {
			for (Unit unit : army) {
				map[unit.x][unit.y] = unit;
			}
		}
		
		for (ArrayList<Unit> army : armies) {
			Collections.sort(army, new Comparator<Unit>() {
				@Override
				public int compare(Unit one, Unit other) {
					int initiative_difference = other.initiative - one.initiative;
					if (initiative_difference != 0) return initiative_difference;
					return one.id - other.id;
				}
			});
			cur_unit.add(0);
		}
		
		cur_army = armies.get(0).get(0).initiative >= armies.get(1).get(0).initiative ? 0 : 1;
	}
	
	public Move getNextMove()
	{
		Unit unit = getAliveUnitInArmy();
		ArrayList<MapObject> move =  new ArrayList<MapObject>();
		ArrayList<MapObject> attack =  new ArrayList<MapObject>();
		for (int x = Math.max(0, unit.x - unit.speed); x <= Math.min(max_x - 1, unit.x + unit.speed); ++x) {
			for (int y = Math.max(0, unit.y - unit.speed); y <= Math.min(max_y - 1, unit.y + unit.speed); ++y) {
				if (map[x][y] == null || (x == unit.x && y == unit.y)) {
					move.add(new MapObject(x, y));
				}
			}
		}
		int range = unit.getAtackRange();
		for (int x = Math.max(0, unit.x - range); x <= Math.min(max_x - 1, unit.x + range); ++x) {
			for (int y = Math.max(0, unit.y - range); y <= Math.min(max_y - 1, unit.y + range); ++y) {
				if (map[x][y] instanceof Unit) {
					Unit other_unit = (Unit) map[x][y];
					if (other_unit.army_id != cur_army) {
						attack.add(new MapObject(x, y));
					}
				}
			}
		}
		return new Move(unit, move, attack);
	}
	
	public void setNextMove(int x, int y)
	{
		Unit unit = getAliveUnitInArmy();
		if (map[x][y] == null || map[x][y] == unit) {
			Logger.logMove(unit, x, y);
			map[unit.x][unit.y] = null;
			map[x][y] = unit;
			unit.x = x;
			unit.y = y;
		} else {
			Unit other = (Unit) map[x][y];
			unit.attack(other);
			if (!other.is_alive) {
				map[other.x][other.y] = null;
			}
			updateIsFinished();
		}
		cur_unit.set(cur_army, (cur_unit.get(cur_army) + 1) % armies.get(cur_army).size()); 
		cur_army = ARMIES_NUMB - cur_army - 1;
	}
	
	private Unit getAliveUnitInArmy()
	{
		while (true) {
			Unit unit = curUnit();
			if (unit.is_alive) {
				return unit;
			} else {
				cur_unit.set(cur_army, cur_unit.get(cur_army) + 1);
			}
		}
	}
	
	private Unit curUnit()
	{
		return armies.get(cur_army).get(cur_unit.get(cur_army) % armies.get(cur_army).size());
	}

	private void updateIsFinished() {
		for (ArrayList<Unit> army : armies) {
			boolean full_army_dead = true;
			for (Unit unit : army) {
				if (unit.is_alive) full_army_dead = false;
			}
			if (full_army_dead) {
				is_finished = true;
				Logger.logEndOfBattle();
			}
		}
	}
}

enum TestUnitsType {
	MELEE,
	RANGE,
	ALL,
	STONE_GOLEM
}

class TestInterface
{
	private static final int MAX_ID = 1000 * 1000;
	
	private static PrintStream out;
	private static ArrayList<MapObject> mapObj;
	private static int M, N;
	private static ArrayList<Integer> ids;
	
	public static void setStream(PrintStream out)
	{
		TestInterface.out = out;
	}
	
	private static MapObject genRandMapObj()
	{
		MapObject obj;
		do {
			obj = new MapObject((int) (Math.random() * M), (int) (Math.random() * N));
		} while (mapObj.contains(obj));
		return obj;
	}
	
	private static int genId()
	{
		Integer id;
		do {
			id = (int) (Math.random() * MAX_ID);
		} while (ids.contains(id));
		return id;
	}
	
	public static ArrayList<Barrier> genAndPrintMap(int M, int N, int K)
	{
		ArrayList<Barrier> barriers = new ArrayList<Barrier>();
		mapObj = new ArrayList<MapObject>();
		ids = new ArrayList<Integer>();
		
		TestInterface.M = M;
		TestInterface.N = N;
		
		out.println("[map]");
		out.println(M + " " + N);
		out.println(K);
		for (int i = 0; i < K; i++) {
			Barrier barrier = genRandBarrier();
			out.println(barrier.x + " " + barrier.y);
			mapObj.add(barrier);
			barriers.add(barrier);
		}
		return barriers;
	}
	
	private static Barrier genRandBarrier() {
		int INDENT_OF_BARRIERS = 4;
		MapObject barrier;
		do {
			barrier = genRandMapObj();
		} while (distToCenter(barrier) < INDENT_OF_BARRIERS);
		return new Barrier(barrier.x, barrier.y);
	}

	private static Unit genRandUnit(int army_id, TestUnitsType units_type)
	{
		MapObject unit_map_obj = genRandMapObj();
		mapObj.add(unit_map_obj);
		Unit unit = UnitFactory.genUnit(genRandName(units_type), army_id, unit_map_obj.x, unit_map_obj.y, genId());
		return unit;
	}
	
	private static String genRandName(TestUnitsType units_type) {
		if (units_type == TestUnitsType.STONE_GOLEM) return "Stone Golem";
		String name;
		Object names[] = ConfigStorage.units.keySet().toArray();
		while (true) {
			name = (String) names[(int) (Math.random() * names.length)];
			UnitConfig config = ConfigStorage.units.get(name);
			switch (units_type) {
				case MELEE:
					if (config.range == ConfigStorage.NOT_DEFINED) return name;
					break;
				case RANGE:
					if (config.range != ConfigStorage.NOT_DEFINED) return name;
					break;
				default:
					return name;
			}
		}
	}

	public static ArrayList<Unit> genAndPrintArmy(int army_id, int army_size, TestUnitsType units_type)
	{
		ArrayList<Unit> army = new ArrayList<Unit>();
		out.println("[army" + (army_id + 1) + "]");
		out.println(army_size);
		while (army_size-- > 0) {
 			Unit unit = genRandUnit(army_id, units_type);
			army.add(unit);
			out.println(unit.x + " " + unit.y + " " + unit.id + " " + unit.name);
		}
		return army;
	}
	
	private static double distToCenter(MapObject obj)
	{
		double x = obj.x - M / 2;
		double y = obj.y - N / 2;
		return Math.sqrt(x*x + y*y);
	}
	
	public static MapObject genAndPrintMove(Move move)
	{
		MapObject obj;
		if (move.attack.isEmpty()) {
			MapObject best_move = move.move.get(0);
			for (MapObject _move : move.move) {
				if (distToCenter(best_move) > distToCenter(_move)) {
					best_move = _move;
				}
			}
			obj = best_move;
		} else {
			obj = move.attack.get( (int) (move.attack.size() * Math.random()) );
		}
		out.println(obj.x + " " + obj.y);
		return obj;
	}
	
	public static void printInput()
	{
		out.println("[input]");
	}
}

public class Hmm {
	public static void genTest(int test_id, int M, int N, int K, int a1_size, int a2_size, TestUnitsType units_type) 
	{
		try {
			PrintStream fin = new PrintStream(new FileOutputStream("tests/00" + test_id + ".dat", false));
			PrintStream fout = new PrintStream(new FileOutputStream("tests/00" + test_id + ".ans", false)); 
			
			TestInterface.setStream(fin);
			Logger.setStream(fout);
			GameInterface.setStream(fout);
			
			ArrayList<Barrier> barriers = TestInterface.genAndPrintMap(M, N, K);
			ArrayList<Unit> army_1 = TestInterface.genAndPrintArmy(0, a1_size, units_type);
			ArrayList<Unit> army_2 = TestInterface.genAndPrintArmy(1, a2_size, units_type);
			TestInterface.printInput();
			Game game = new Game(M, N, barriers, army_1, army_2);
			while (!game.is_finished) {
				Move move = game.getNextMove();
				GameInterface.printMove(move);
				MapObject obj = TestInterface.genAndPrintMove(move);
				game.setNextMove(obj.x, obj.y); 
			}
			
			fin.close();
			fout.close();
		} catch (Exception e) {
			System.out.println("ERROR");
		}
		
	}
	
	public static void main(String[] args) {
		genTest(5, 10, 10, 10, 3, 3, TestUnitsType.STONE_GOLEM);
	}
	
	@Test
	public void testHmm()
	{
		for (TestUnitsType units_type : TestUnitsType.values()) {
			genTest(1, 10, 10, 5, 1, 1, units_type);
		}
		
	}
	
	@Test
	public void testMonkGriffinAttack()
	{
		Unit monk = UnitFactory.genUnit("Monk", 1, 0, 0, 1);
		Unit griffin = UnitFactory.genUnit("Griffin", 0, 1, 0, 2);
		assertEquals(17, monk.weapon);
		assertEquals(8, griffin.armor);
		assertEquals(7, monk.damage(griffin));
	}

}


